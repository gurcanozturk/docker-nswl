FROM centos:7.4.1708

RUN yum --assumeyes install glibc.i686 expect && yum clean all && expect -version

ENV NSWL_RPM=nswl_linux-*.rpm
COPY $NSWL_RPM /
ENV NSWL /usr/local/netscaler/bin/nswl
RUN rpm --install $NSWL_RPM && rm $NSWL_RPM && $NSWL -version

CMD ["/usr/local/netscaler/bin/nswl", "-start", "-f", "/usr/local/netscaler/etc/sample-log.conf"]
